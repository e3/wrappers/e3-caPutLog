require caputlog,${E3_MODULE_VERSION}

epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")
epicsEnvSet(HOST, "localhost")

# Access security file cfg needed for caputlog
asSetFilename("$(TOP)/asg.acf")
dbLoadRecords("$(TOP)/db/pv_test.db")
# TODO: use afterInit instead of iocInit
iocInit()
caPutJsonLogInit("$(HOST)", 0)
caPutJsonLogAddMetadata("Meta", "Data")
