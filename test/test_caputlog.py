import json
import logging
import threading
import socketserver
from os import environ
from pathlib import Path
from pprint import pformat

import pytest
from epics import PV
from run_iocsh import IOC

CMD_FILE = "cmd/pv_test.cmd"
HOST = "127.0.0.1"
PORT = 7011
LOGGER = logging.getLogger(__name__)
EVENT = threading.Event()
pytest.LOG_MSG = ""


# LogServerHandle will be used when creating the TCP server.
# Overwritting the handle method will allows us to handle the requests
# that the TCP server receives from caputlog. It will save the data and
# signal when the message/data is ready so the "EVENT.wait()" in
# the main Thread (test_*) is unlocked, then the json data is parsed there.
class LogServerHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = bytes.decode(self.request.recv(1024).strip())
        LOGGER.debug("Receiving message:\n" + pformat(json.loads(data), indent=4))
        LOGGER.debug("Length in bytes " + str(len(data)))
        pytest.LOG_MSG = data
        EVENT.set()


@pytest.fixture(scope="session")
def start_logger_server():
    LOGGER.info("Starting log server")
    socketserver.TCPServer.allow_reuse_address = True
    server = socketserver.TCPServer((HOST, PORT), LogServerHandler)
    t = threading.Thread(target=server.serve_forever)
    t.start()
    yield
    LOGGER.info("Shutting down log server")
    server.shutdown()
    t.join()


@pytest.fixture(scope="session")
def handle_ioc(start_logger_server):
    LOGGER.info("Starting IOC")
    test_cmd = Path(__file__).parent / CMD_FILE
    args = ["-l", str(environ.get("TEMP_CELL_PATH")), str(test_cmd)]
    ioc = IOC(*args, timeout=20.0)
    ioc.start()
    yield ioc
    LOGGER.info("Shutting down IOC")
    ioc.exit()


@pytest.mark.parametrize(
    "record_type, json_keys, value",
    [
        (
            "stringout_string",
            ["Meta", "date", "host", "new", "old", "pv", "time", "user"],
            "Test_string",
        ),
        (
            "waveform_string",
            [
                "Meta",
                "date",
                "host",
                "new",
                "old",
                "pv",
                "time",
                "user",
                "new-size",
                "old-size",
            ],
            ["str1", "str2"],
        ),
    ],
)
def test_check_json_structure_headers(record_type, json_keys, value, handle_ioc):
    PV(record_type).put(value)
    EVENT.wait()
    received_msg = json.loads(pytest.LOG_MSG)
    EVENT.clear()

    for keys in received_msg.keys():
        assert keys in json_keys


@pytest.mark.parametrize(
    "pv_name, value",
    [
        ("stringout_string", "New_string"),
        ("longout_long", 1334567890),
        ("ao_float", 7.902),
        ("mbbo_enum", 2),
        (
            "waveform_string",
            [
                "string12345678901234567890123456789012",
                "string123456789012345678901234567890123",
            ],
        ),
        (
            "waveform_double",
            [
                19.1,
                119.11,
                229.22,
                339.33,
                449.44,
                559.55,
                669.66,
                779.77,
                889.88,
                999.99,
            ],
        ),
    ],
)
def test_check_new_pv_values(pv_name, value, handle_ioc):
    PV(pv_name).put(value)
    EVENT.wait()
    received_msg = json.loads(pytest.LOG_MSG)
    EVENT.clear()

    assert received_msg["new"] == value


def test_check_metadata(handle_ioc):
    PV("stringout_string").put("New_string")
    EVENT.wait()
    received_msg = json.loads(pytest.LOG_MSG)
    EVENT.clear()
    assert received_msg["Meta"] == "Data"
