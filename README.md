
# e3-caPutLog  

Wrapper for `caPutLog`.

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

Through use of `essioc`.

## Metadata

E3 caputlog has the capability of adding metadata to the json output.
To use it you just need to call caPutJsonLogAddMetadata passing the property and value.

```sh
caPutJsonLogMetgadata <property> <value>
```

The value will be overwriten if you call multiple times for the same property.
 
## Contributing

Contributions through pull/merge requests only.

